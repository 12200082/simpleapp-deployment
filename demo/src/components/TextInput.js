import React from "react";
import { css } from "styled-components";

function TextInput({isValid, notValid, inputConfig}) {
    let cssClass = 'input-default';

    if (notValid) {
        cssClass = 'input-recommended';
    }
    if (!isValid) {
        cssClass = 'input-invalid';
    }

    return (
        <input
            className={cssClass}
            type={inputConfig.type}
            placeholder={inputConfig.placeholder}
            value={inputConfig.value}
            label={inputConfig.value.label}
            onChange={inputConfig.onChange}
            
        />
    );
}

export default TextInput;
