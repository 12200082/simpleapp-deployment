import React, { useState } from "react";

function LoginForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState(false);

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Perform login validation here
    if (email === "example@mail.com") {
      setEmailError(false);
      console.log("Login successful");
    } else {
      setEmailError(true);
      console.log("Incorrect email");
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div>
        <label>Email</label>
          <input
            
            type="email"
            value={email}
            onChange={handleEmailChange}
            style={{ borderColor: emailError ? "red" : "" }}
          />
        </div>
        <div>
          <input
            type="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default LoginForm;
