import React, {useState} from "react";
import TextInput from "./TextInput";
export function Login(){
    const [inputValue, setInputValue] = useState("");
    const [isValid, setIsValid] = useState(true);
    const [notValid, setNotValid] = useState(false);

    const handleInputChange = (event) => {
        setInputValue(event.target.value);

        setIsValid(event.target.value.trim().length > 0);

    }
    const handleCheckboxChange = (event) => {
        setNotValid(event.target.checked);
    }
    return (
        <div>
            <TextInput
                isValid={isValid}
                notValid ={notValid}
                inputConfig = {{
                    type: 'text',
                    label: 'Email',
                    placeholder: 'Enter email',
                    value: inputValue,
                    onchange: handleInputChange,
                    required: true
                }}
            />
            <div>
                <input
                    type="text"
                    id="recommend-checkbox"
                    checked={notValid}
                    onChange={handleCheckboxChange}
                />
            </div>
        </div>
    )


}