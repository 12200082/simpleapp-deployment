import logo from './logo.svg';
import './App.css';
import { ConditionalStyle } from './components/ConditionalStyle';
import { Login } from './components/Login';
import LoginForm from './components/LoginForm';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <div><ConditionalStyle/></div>
        <div><Login/></div>
        <div><LoginForm/></div>
       
      </header>
      
    </div>
    
  );
}

export default App;
